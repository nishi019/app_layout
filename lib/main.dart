import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:frino_icons/frino_icons.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget titleSelection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text('Achievement of Naomi Osaka',
                    style: TextStyle(fontWeight: FontWeight.bold)
                  ),
                ),
              ],
            ),
          ),
          Icon(FrinoIcons.f_thumb_up__1_, color: Colors.redAccent),
          Text('1.7M'),
        ],
      ),
    );
    Color color = Theme
        .of(context)
        .primaryColorDark;
    Column _buildButtonColumn(Color color, IconData icon, String label) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(icon, color: color),
          Container(
              margin: const EdgeInsets.only(top: 8),
              child: Text(
                  label,
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    color: color,
              )
      ),
          ),
      ],
      );
    }
    Widget buttonSection = Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            _buildButtonColumn(color, FrinoIcons.f_trophy, 'Titles'),
            _buildButtonColumn(color, FrinoIcons.f_piggy_bank__1_, 'Prize'),
            _buildButtonColumn(color, FrinoIcons.f_news, 'Results'),
            _buildButtonColumn(color, FrinoIcons.f_calendar, 'Tournament')
          ],
        )
    );
    Widget textSection = Container(
        padding: const EdgeInsets.all(32),
        child: Text(
          'Naomi Osaka is one of my most favorite tennis players in the world.'
              'I created this app just because she won the UP Open few days ago '
              'and I want to celebrate her and her achievement. '
              '(I just want to create app using her awesome picture..! ) '
              'CONGRATS NAOMI!!!!!!!',

          softWrap: true,
        )
    );

    return MaterialApp(
        title: 'Layout Homework',
        theme: ThemeData(
          textTheme: GoogleFonts.mcLarenTextTheme(
            Theme.of(context).textTheme,
          )
        ),
        home: Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.green,
              title: Text('Layout Homework',
              style: GoogleFonts.mcLaren()),
            ),
            body: ListView(
              children: [
                Image.asset(
                    'image/naomiosaka.jpeg', width: 600, height: 240, fit: BoxFit.cover),
                titleSelection,
                buttonSection,
                textSection
              ],
            )
        )
    );
  }
}